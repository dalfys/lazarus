msgid ""
msgstr ""
"Project-Id-Version: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Maxim Ganetsky <maxkill@mail.ru>\n"
"Language-Team: \n"
"Language: ru\n"
"X-Generator: Poedit 2.4.3\n"

#: ideconfstrconsts.liscomponentnameisapascalkeyword
#, object-pascal-format
msgid "Component name \"%s\" is a Pascal keyword."
msgstr "Имя компонента \"%s\" является ключевым словом Паскаля."

#: ideconfstrconsts.liscomponentnameisnotavalididentifier
#, object-pascal-format
msgid "Component name \"%s\" is not a valid identifier"
msgstr "Имя компонента \"%s\" не является корректным идентификатором"

#: ideconfstrconsts.listmfunctionappendpathdelimiter
msgid "Function: append path delimiter"
msgstr "Функция: добавление разделителя пути"

#: ideconfstrconsts.listmfunctionchomppathdelimiter
msgid "Function: remove trailing path delimiter"
msgstr "Функция: удаление завершающего разделителя пути"

#: ideconfstrconsts.listmfunctionextractfileextension
msgid "Function: extract file extension"
msgstr "Функция: извлечение расширения файла"

#: ideconfstrconsts.listmfunctionextractfilenameextension
msgid "Function: extract file name+extension"
msgstr "Функция: извлечение имени файла с расширением"

#: ideconfstrconsts.listmfunctionextractfilenameonly
msgid "Function: extract file name only"
msgstr "Функция: извлечение только имени файла"

#: ideconfstrconsts.listmfunctionextractfilepath
msgid "Function: extract file path"
msgstr "Функция: извлечение пути к файлу"

#: ideconfstrconsts.listmunknownmacro
#, object-pascal-format
msgid "(unknown macro: %s)"
msgstr "(неизвестный макрос: %s)"

